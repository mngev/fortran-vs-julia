"Суммирование векторов без использования векторизации"
function iter_sum(A::Vector{Float64}, B::Vector{Float64})::Vector{Float64}
  N = length(A)
  C = Vector{Float64}(N)
  for i in 1:N
    C[i] = A[i] + B[i]
  end
  return C
end

"Суммирование векторов с использованием векторизации"
function vec_sum(A::Vector{Float64}, B::Vector{Float64})::Vector{Float64}
  return A + B
end

"simd инструкции"
function simd_sum(A::Vector{Float64}, B::Vector{Float64})::Vector{Float64}
  N = length(A)
  C = Vector{Float64}(N)
  @simd for i in 1:N
    @inbounds C[i] = A[i] + B[i]
  end
  return C
end

# Считываем аргументы командной строки
const trials = length(ARGS) < 1 ? 100 : parse(Int64, ARGS[1])
const vector_size = length(ARGS) < 2 ? 8 : parse(Int64, ARGS[2])

# @show iterations_num vector_size 

A = rand(vector_size)
B = rand(vector_size)
C = Vector{Float64}(vector_size)

# println("# с векторизацией, без векторизации")
for i in 1:trials
  tic()
  for j in 1:10000
    С = iter_sum(A, B)
  end
  time1 = toq()
  
  tic()
  for j in 1:10000
    С = vec_sum(A, B)
  end
  time2 = toq()

  tic()
  for j in 1:10000
    С = simd_sum(A, B)
  end
  time3 = toq()
  
  @printf "%3i,%G,%G,%G\n" i time1 time2 time3
end
