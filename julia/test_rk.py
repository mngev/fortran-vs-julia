#! /usr/bin/env python3
#-*- coding: utf-8 -*-

import subprocess
import sys
import numpy as np

# Флаги оптимизации
OFLAG = sys.argv[1] if len(sys.argv) > 1 else '-O1'
FILE = f'../data/julia_rk_{OFLAG[1:]}.npy'

execs_num = 100

measurements = []

test = ['julia', 'test_rk.jl']

for exe in range(execs_num):
  run = subprocess.run(test, stdout=subprocess.PIPE)
  stdout = run.stdout.decode('utf-8')
  measurements.append(float(stdout))

measurements = np.array(measurements)

np.save(FILE, measurements)
