#! /usr/bin/env python3
# coding -*- utf-8 -*-
import subprocess
import numpy as np

# Параметры для компиляции
# число запусков программы, число итераций в каждом запуске
# и максимальная длина складываемых векторов
iterations_num = 100
execs_num = 20
compilations_num = 20
vector_size = 8
FILE = '../data/julia.npy'

# Массив для сохранения измерений
# в каждой итерации два замера: 
measurements = np.zeros(shape=(3, execs_num, iterations_num))

for exec_num in range(execs_num):
    print(f'{exec_num}', end=' ', flush=True)
    cmd = ['julia', 'vectorization.jl', str(iterations_num), str(vector_size)]
    print(' '.join(cmd))
    run = subprocess.run(cmd, stdout=subprocess.PIPE)
    stdout = run.stdout.decode('utf-8')

    m_1, m_2, m_3 = np.loadtxt(stdout.splitlines(), delimiter=',', usecols=(1, 2, 3), unpack=True, dtype=np.float64)
    measurements[0, exec_num, :] = m_1
    measurements[1, exec_num, :] = m_2
    measurements[2, exec_num, :] = m_3
        

np.save(FILE, measurements)