include("RK.jl")

"""Правая часть ОДУ линейного осциллятора"""
function oscillator(t::Float64, x::Vector{Float64})::Vector{Float64}
    return [-x[2], x[1]]
end

const h = 1.0e-5
const t_start = 0.0
const t_stop = 10.0
const x_0 = [ 1.0, 0.5 ]

tic()
  RKp6n1(oscillator, x_0, h, t_start, t_stop)
time = toq()

println(time)