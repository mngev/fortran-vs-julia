function RKp6n1(func::Function, x_0::Vector{Float64}, h::Float64, t_start::Float64, t_stop::Float64)::Tuple{Float64,Vector{Float64}}
  
  local t::Float64
  local x::Vector{Float64}
  local const EQN = length(x_0)
  local last_step::Bool
  
  last_step = false
  t = t_start
  x = copy(x_0)
  
  
  local const a21 = 1/2
  local const a31 = 2/9
  local const a32 = 4/9
  local const a41 = 7/36
  local const a42 = 2/9
  local const a43 = -1/12
  local const a51 = -35/144
  local const a52 = -55/36
  local const a53 = 35/48
  local const a54 = 15/8
  local const a61 = -1/360
  local const a62 = -11/36
  local const a63 = -1/8
  local const a64 = 1/2
  local const a65 = 1/10
  local const a71 = -41/260
  local const a72 = 22/13
  local const a73 = 43/156
  local const a74 = -118/39
  local const a75 = 32/195
  local const a76 = 80/39
  
  local const b1 = 13/200
  local const b3 = 11/40
  local const b4 = 11/40
  local const b5 = 4/25
  local const b6 = 4/25
  local const b7 = 13/200
  
  local const c2 = 1/2
  local const c3 = 2/3
  local const c4 = 1/3
  local const c5 = 5/6
  local const c6 = 1/6
  local const c7 = 1
  
  local k1::Vector{Float64} = zeros(Float64, EQN)
  local k2::Vector{Float64} = zeros(Float64, EQN)
  local k3::Vector{Float64} = zeros(Float64, EQN)
  local k4::Vector{Float64} = zeros(Float64, EQN)
  local k5::Vector{Float64} = zeros(Float64, EQN)
  local k6::Vector{Float64} = zeros(Float64, EQN)
  local k7::Vector{Float64} = zeros(Float64, EQN)
  
  while true
    
    k1 = func(t, x)
    k2 = func(t + h*c2, x + h*(k1*a21))
    k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
    k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
    k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
    k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))
    k7 = func(t + h*c7, x + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76))
    
    x = x + h*(b1*k1 + b3*k3 + b4*k4 + b5*k5 + b6*k6 + b7*k7)
    t = t + h
    
    if last_step
      break
    end
    
    if (t + 1.01*h > t_stop)
      h = t_stop - t
      last_step = true
    end
    
  end
  return (t, x)
  
end # RKp6n1