File `benchmarking.py` contains some functions that implement the methodology described in articles:

 - Tomas Kalibera, Richard Jones Rigorous Benchmarking in Reasonable Time
 - Tomas Kalibera, Richard Jones Quantifying Performance Changes with Effect Size Confidence Intervals

 Some test Fortran vs Julia. Statistical processing of the results was performed using Python with NumPy, SciPy stats and Matplotlib.