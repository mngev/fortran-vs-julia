#! /usr/bin/env python3
# coding -*- utf-8 -*-
import subprocess
import sys
import numpy as np
# Параметры для компиляции
# число запусков программы, число итераций в каждом запуске
# и максимальная длина складываемых векторов
iterations_num = 100
execs_num = 20
compilations_num = 20

# Флаги оптимизации
OFLAG = sys.argv[1] if len(sys.argv) > 1 else '-O1'
FILE = f'../data/fortran_{OFLAG[1:]}.npy'

# Массив для сохранения измерений, в каждой итерации два замера: 
measurements = np.zeros(shape=(2, compilations_num, execs_num, iterations_num))

# Уровень эксперимента 3 (компиляция/бинарники)
for compilation_num in range(compilations_num):
    print(f'\nКомпиляция {compilation_num}:', end=' ')
    # Команды для компиляции
    build01 = ['make', 'clean']
    build02 = ['make', f'BFLAGS={OFLAG}']
    compilation01 = subprocess.run(build01, stderr=subprocess.PIPE)
    compilation02 = subprocess.run(build02, stderr=subprocess.PIPE)

    if compilation01.returncode != compilation02.returncode != 0:
        print(compilation01.stderr.decode('utf-8'))
        print(compilation02.stderr.decode('utf-8'))
    else:
        # Компиляция прошла успешно
        # Уровень эксперимента 2 (выполнение)
        for exec_num in range(execs_num):
            print(f'{exec_num} ', end=' ', flush=True)
            run = subprocess.run(['./bin/vectorization'], stdout=subprocess.PIPE)
            stdout = run.stdout.decode('utf-8')
            
            # Уровень эксперимента 1 (итерации)
            it, vec = np.loadtxt(stdout.splitlines(), delimiter=',', unpack=True, dtype=np.float64)
            # Сохраняем результаты замеров
            measurements[0, compilation_num, exec_num, :] = it
            measurements[1, compilation_num, exec_num, :] = vec

print(f"Сохраняем в {FILE}.npy")
np.save(FILE, measurements)
