module RK
  use iso_fortran_env, only: int32, int64, real32, real64
  implicit none
  
  abstract interface
    pure function ode_func(t, x)
      double precision, intent(in)               :: t ! Время
      double precision, dimension(:), intent(in) :: x ! Переменные
      double precision, dimension(1:ubound(x,1)) :: ode_func
    end function ode_func
  end interface
  
  private

! Доступные вовне функции и подпрограммы
  public :: RKp6n1, oscillator, oscillator_exact
  
  contains
  
!-------------------------------------------------------------------------------
!  Гармонический осциллятор
!-------------------------------------------------------------------------------
  pure function oscillator(t, x)
    implicit none
    real(real64), intent(in) :: t
    real(real64), dimension(:), intent(in) :: x
    real(real64), dimension(1:ubound(x,1)) :: oscillator
    !==
    oscillator(1) = -x(2)
    oscillator(2) = +x(1)
  end function oscillator
  
  
  ! Точное решение для oscillator
  function oscillator_exact(x, t)
    implicit none
    real(real64), dimension(:), intent(in) :: x
    real(real64), intent(in) :: t
    real(real64), dimension(1:ubound(x,1)) :: oscillator_exact
    !== Начальные значения (xo, yo)
    oscillator_exact(1) = x(1) * cos(t) - x(2) * sin(t)
    oscillator_exact(2) = x(2) * cos(t) + x(1) * sin(t)
  end function oscillator_exact
  
  
  !-------------------------------------------------------------------------
  pure function RKp6n1(func, x_0, step, t_start, t_stop)
    implicit none
    procedure(ode_func) :: func
    real(real64), dimension(1:), intent(in) :: x_0
    real(real64),intent(in) :: step, t_start, t_stop
    real(real64), dimension(1:size(x_0)) :: RKp6n1
    
    real(real64), parameter :: &
    a21 = 0.5, a31 = 2.0/9.0, a32 = 4.0/9.0, a41 = 7.0/36.0, &
    a42 = 2.0/9.0, a43 = -1.0/12.0, a51 = -35.0/144.0, a52 = -55.0/36.0, &
    a53 = 35.0/48.0, a54 = 15.0/8.0, a61 = -1.0/360.0, a62 = -11.0/36.0, &
    a63 = -1.0/8.0, a64 = 0.5, a65 = 0.1, a71 = -41.0/260.0, a72 = 22.0/13.0, &
    a73 = 43.0/156.0, a74 = -118.0/39.0, a75 = 32.0/195.0, a76 = 80.0/39.0
    
    real(real64), parameter :: &
    b1 = 13.0/200.0, b3 = 11.0/40.0, b4 = 11.0/40.0, b5 = 4.0/25.0, &
    b6 = 4.0/25.0, b7 = 13.0/200.0
    
    real(real64), parameter :: &
    c2 = 0.5, c3 = 2.0/3.0, &
    c4 = 1.0/3.0, c5 = 5.0/6.0, &
    c6 = 1.0/6.0, c7 = 1.0

    logical :: last_step
    real(real64), dimension(1:size(x_0)) :: x
    real(real64), dimension(1:size(x_0)) :: k1, k2, k3, k4, k5, k6, k7
    real(real64) :: t, h
    integer(int32) :: EQN 
    EQN = size(x_0)
    
    last_step = .false.
    t = t_start
    h = step
    x = x_0
    k1 = 0.0; k2 = 0.0; k3 = 0.0; k4 = 0.0
    k5 = 0.0; k6 = 0.0; k7 = 0.0
    
    do while (.true.) !(t <= t_stop)
      k1 = func(t, x)
      k2 = func(t + h*c2, x + h*(k1*a21))
      k3 = func(t + h*c3, x + h*(k1*a31 + k2*a32))
      k4 = func(t + h*c4, x + h*(k1*a41 + k2*a42 + k3*a43))
      k5 = func(t + h*c5, x + h*(k1*a51 + k2*a52 + k3*a53 + k4*a54))
      k6 = func(t + h*c6, x + h*(k1*a61 + k2*a62 + k3*a63 + k4*a64 + k5*a65))
      k7 = func(t + h*c7, x + h*(k1*a71 + k2*a72 + k3*a73 + k4*a74 + k5*a75 + k6*a76))
      
      x = x + h * (b1 * k1 + b3 * k3 + b4 * k4 + b5 * k5 + b6 * k6 + b7 * k7)
      t = t + h
      
      if (last_step) then
        exit
      end if
      
      if (t + 1.01 * h > t_stop) then
        h = t_stop - t
        last_step = .true.
      end if
    end do
    
    RKp6n1 = x
  end function RKp6n1
end module RK
