! Программа иллюстрирует векторизацию арифметических
! действий над небольшими массивами.
include "test_functions.f90"
program vectorization
  use iso_fortran_env, only: int32, int64, real32, real64
  use test_functions
  implicit none

  integer, parameter :: trials = 100
  integer(int64), parameter :: array_size = 8
  real(real64), allocatable, dimension(:) :: A, B, C
  integer(int64) :: i, tn
  real(real32) :: t1, t2, t

  allocate(A(array_size), B(array_size), C(array_size))

  call random_number(A)
  call random_number(B)
  call random_number(C)

  do tn = 1,trials,1
    
    call cpu_time(t1)
    do i = 1,10000,1
      C = iter_sum(A, B)
    end do
    call cpu_time(t2)
    
    t = t2 - t1
    
    call cpu_time(t1)
    do i = 1,10000,1
      C = vec_sum(A, B)
    end do
    call cpu_time(t2)
    
    print '(G0,",",G0)', t, (t2 - t1)
  end do

  deallocate(A, B, C)
end program vectorization
