module test_functions
  use iso_fortran_env, only: int32, int64, real32, real64
  implicit none
  private

! Доступные вовне функции и подпрограммы
  public :: iter_sum, vec_sum
  
  contains
  ! Суммирование с итерациями
  pure function iter_sum(A, B) result(res)
    implicit none
    real(kind=8), intent(in), dimension(1:) :: A, B
    real(kind=8), dimension(1:size(A)) :: res
    integer(kind=4) :: array_size, i

    array_size = size(A)
    do concurrent(i = 1:array_size)
      res(i) = A(i) + B(i)
    end do 
  end function
  
  ! Суммирование с векторизацией
  pure function vec_sum(A, B) result(res)
    implicit none
    real(kind=8), intent(in), dimension(1:) :: A, B
    real(kind=8), dimension(1:size(A)) :: res
    res = A + B
  end function
  
end module test_functions
