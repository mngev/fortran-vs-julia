include "RK.f90"

program test_rk
  use iso_fortran_env, only: int32, int64, real32, real64
  use rk
  implicit none
  real(real64), parameter :: h = 1d-5
  real(real64), parameter :: t_start = 0.0
  real(real64), parameter :: t_stop = 10.0
  real(real64), dimension(1:2), parameter :: x_0 = [ 1.0d0, 0.5d0 ]
  real(real64), dimension(1:2) :: x
  
  integer, parameter :: trials = 100
  real(real32) :: t1, t2

  call cpu_time(t1)
  x =  RKp6n1(oscillator, x_0, h, t_start, t_stop)
  call cpu_time(t2)

  print *, t2 - t1
  
end program test_rk