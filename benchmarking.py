# coding -*- utf-8 -*-
""" Functions that implement the methodology described in articles:
 - Tomas Kalibera, Richard Jones Rigorous Benchmarking in Reasonable Time
 - Tomas Kalibera, Richard Jones Quantifying Performance Changes with Effect Size Confidence Intervals
"""
import numpy as np
import scipy.stats


def acorr(data, lag: int):
    """Autocorrelation of `data` with `lag`"""
    if lag == 0:
        res = 1
    else:
        μ = np.mean(data[:-lag])
        σ2 = np.var(data[:-lag])
        res = np.mean((data[:-lag] - μ) * (data[lag:] - μ)) / σ2
    return res


def MVs(data, calcs_order: str):
    """Consistent calculation of mean and unbiased sample variance 
    from the `data` multidimensional array in order, specified in 
    `calcs_order` string"""
    res = data
    for s in reversed(calcs_order):
        if s == 'M':
            res = np.mean(res, axis=-1)
        elif s == 'V':
            res = np.var(res, ddof=1, axis=-1)
        elif s == '*':
            pass
        else:
            raise ValueError('Error in `calc_order` string!')
    return res


def iteration_num(i: int, data):
    """Calculate r_i value from `data` array"""
    levels = np.shape(data)
    total_levels = len(levels)
    if i > total_levels:
        raise ValueError(f'There are only {total_levels} levels!')
    return levels[total_levels - i]


def S2(i: int, data):
    """The biased estimator of variance at level `i`
    $S^2_i, i = 1,..,n+1$"""
    length = len(np.shape(data))
    if length < 1:
        raise ValueError('Number of levels must > 2')
    if i > length:
        raise ValueError('i > len(np.shape(data))')
    # Forming the order of calculations
    order = (length - i) * 'M' + 'V' + (i - 1) * 'M' 
    return MVs(data, order)


def T2(i: int, data):
    """Iterative calculation of The unbiased estimator 
    of variance at level `i`. $T^2_i, i=1,..,n+1$"""
    if i == 1:
        return S2(1, data)
    else:
        return S2(i, data) - S2(i-1, data) / iteration_num(i-1, data)


def conf_Δ(data, α=0.01, k=2):
    """Confidence interval, k -- degrees of freedom
    k must be n - 1, where n is number of experiment levels"""
    t = scipy.stats.t(df=k).ppf(1 - α / 2)
    return t * np.sqrt(S2(k+1, data) / iteration_num(k+1, data))


def optimum(data, i: int, c:int=1):
    """Optimum number of measurements for level `i`
    `с` is number of tests required for warm up"""
    res = T2(i, data) / T2(i+1, data)
    res = np.abs(c * res)
    res = np.sqrt(res)
    return np.ceil(res)